rem set publication=dostoevski-v2
rem set publication=pushkin
rem set publication=19vek\zhukovsky
rem set publication=tolstoy
rem set publication=leskov
rem set publication=saltykov-shchedrin
rem set publication=18vek\poety1790_1810
rem set publication=18vek\novikov
set publication=tolstoy

set root=\\G780\rvb
set database=id$.mem

c:\xmwin\bin\xm -p analyze-ids.par -f%root%\%publication%\*.htm -r
pause